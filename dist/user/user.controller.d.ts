import { UserService } from './user.service';
import { UserModel } from './model/user';
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    showMe(username: string): Promise<import("./user.entity").User>;
    register(newUser: UserModel, response: any): void;
    login(data: UserModel): Promise<UserModel>;
}
