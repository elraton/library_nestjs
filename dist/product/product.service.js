"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const product_entity_1 = require("./product.entity");
const typeorm_2 = require("typeorm");
const category_entity_1 = require("../category/category.entity");
const brand_entity_1 = require("../brand/brand.entity");
const store_entity_1 = require("../store/store.entity");
let ProductService = class ProductService {
    constructor(productRepository, categoryRepository, brandRepository, storeRepository) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.brandRepository = brandRepository;
        this.storeRepository = storeRepository;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.productRepository.find();
        });
    }
    create(newData) {
        return __awaiter(this, void 0, void 0, function* () {
            const newcategory = new product_entity_1.Product();
            newcategory.name = newData.name;
            newcategory.code = newData.code;
            newcategory.image = newData.image;
            newcategory.price = newData.price;
            newcategory.unit_price = newData.unit_price;
            newcategory.quantity = newData.quantity;
            newcategory.description = newData.description;
            newcategory.category = yield this.categoryRepository.findOneOrFail(newData.category);
            newcategory.brand = yield this.brandRepository.findOneOrFail(newData.brand);
            newcategory.store = yield this.storeRepository.findOneOrFail(newData.store);
            return this.productRepository.save(newcategory);
        });
    }
    update(idproduct, updateData) {
        return __awaiter(this, void 0, void 0, function* () {
            const product = yield this.productRepository.findOneOrFail(idproduct);
            product.name = updateData.name;
            product.code = updateData.code;
            product.image = updateData.image;
            product.price = updateData.price;
            product.unit_price = updateData.unit_price;
            product.quantity = updateData.quantity;
            product.description = updateData.description;
            product.category = yield this.categoryRepository.findOneOrFail(updateData.category);
            product.brand = yield this.brandRepository.findOneOrFail(updateData.brand);
            product.store = yield this.storeRepository.findOneOrFail(updateData.store);
            return this.productRepository.save(product);
        });
    }
    delete(idproduct) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.productRepository.delete(idproduct);
        });
    }
};
ProductService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(product_entity_1.Product)),
    __param(1, typeorm_1.InjectRepository(category_entity_1.Category)),
    __param(2, typeorm_1.InjectRepository(brand_entity_1.Brand)),
    __param(3, typeorm_1.InjectRepository(store_entity_1.Store)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], ProductService);
exports.ProductService = ProductService;
//# sourceMappingURL=product.service.js.map