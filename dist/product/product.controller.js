"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const product_1 = require("./model/product");
const product_service_1 = require("./product.service");
const express_1 = require("express");
const auth_guard_1 = require("../auth.guard");
let ProductController = class ProductController {
    constructor(productService) {
        this.productService = productService;
    }
    findAll(response) {
        this.productService.findAll().then(data => {
            response.status(common_1.HttpStatus.OK).json(data);
        }).catch(error => {
            response.status(common_1.HttpStatus.FORBIDDEN).json({ msg: 'No se pudo obtener la data' });
        });
    }
    create(newProduct, response) {
        this.productService.create(newProduct).then(data => {
            response.status(common_1.HttpStatus.CREATED).json(data);
        }).catch(error => {
            response.status(common_1.HttpStatus.FORBIDDEN).json({ msg: 'No se pudo crear la data' });
        });
    }
    update(updateProduct, response, idProduct) {
        this.productService.update(idProduct, updateProduct).then(data => {
            response.status(common_1.HttpStatus.OK).json(data);
        }).catch(error => {
            response.status(common_1.HttpStatus.FORBIDDEN).json({ msg: 'No se pudo actualizar la data' });
        });
    }
    delete(idProduct) {
        this.productService.delete(idProduct).then(data => {
            express_1.response.status(common_1.HttpStatus.OK).json(data);
        }).catch(error => {
            express_1.response.status(common_1.HttpStatus.FORBIDDEN).json({ msg: 'No se pudo actualizar la data' });
        });
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(new auth_guard_1.AuthGuard()),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ProductController.prototype, "findAll", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(new auth_guard_1.AuthGuard()),
    __param(0, common_1.Body()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [product_1.ProductModel, Object]),
    __metadata("design:returntype", void 0)
], ProductController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(new auth_guard_1.AuthGuard()),
    __param(0, common_1.Body()), __param(1, common_1.Res()), __param(2, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [product_1.ProductModel, Object, Object]),
    __metadata("design:returntype", void 0)
], ProductController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(new auth_guard_1.AuthGuard()),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ProductController.prototype, "delete", null);
ProductController = __decorate([
    common_1.Controller('product'),
    __metadata("design:paramtypes", [product_service_1.ProductService])
], ProductController);
exports.ProductController = ProductController;
//# sourceMappingURL=product.controller.js.map