import { Product } from './product.entity';
import { Repository } from 'typeorm';
import { ProductModel } from './model/product';
import { Category } from '../category/category.entity';
import { Brand } from '../brand/brand.entity';
import { Store } from '../store/store.entity';
export declare class ProductService {
    private readonly productRepository;
    private readonly categoryRepository;
    private readonly brandRepository;
    private readonly storeRepository;
    constructor(productRepository: Repository<Product>, categoryRepository: Repository<Category>, brandRepository: Repository<Brand>, storeRepository: Repository<Store>);
    findAll(): Promise<Product[]>;
    create(newData: ProductModel): Promise<Product>;
    update(idproduct: number, updateData: ProductModel): Promise<Product>;
    delete(idproduct: number): Promise<import("typeorm").DeleteResult>;
}
