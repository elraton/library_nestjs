import { Category } from '../category/category.entity';
import { Brand } from '../brand/brand.entity';
import { Store } from '../store/store.entity';
export declare class Product {
    id: number;
    image: string;
    name: string;
    code: string;
    price: number;
    unit_price: number;
    quantity: number;
    description: string;
    category: Category;
    brand: Brand;
    store: Store;
}
