import { BrandModel } from './model/brand';
import { Repository } from 'typeorm';
import { Brand } from './brand.entity';
export declare class BrandService {
    private readonly brandRepository;
    constructor(brandRepository: Repository<Brand>);
    findAll(): Promise<Brand[]>;
    create(newData: BrandModel): Promise<Brand>;
    update(idbrand: number, updateData: BrandModel): Promise<Brand>;
    delete(idBrand: number): Promise<import("typeorm").DeleteResult>;
}
