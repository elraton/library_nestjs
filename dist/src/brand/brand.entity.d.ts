import { Product } from '../product/product.entity';
export declare class Brand {
    id: number;
    name: string;
    status: string;
    products: Product[];
}
