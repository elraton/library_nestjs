import { StoreModel } from './model/store';
import { StoreService } from './store.service';
export declare class StoreController {
    private storeService;
    constructor(storeService: StoreService);
    findAll(response: any): void;
    create(newStore: StoreModel, response: any): void;
    update(updateStore: StoreModel, response: any, idStore: any): void;
    delete(response: any, idStore: any): void;
}
