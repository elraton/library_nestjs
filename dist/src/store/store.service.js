"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const store_entity_1 = require("./store.entity");
const typeorm_2 = require("typeorm");
let StoreService = class StoreService {
    constructor(storeRepository) {
        this.storeRepository = storeRepository;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.storeRepository.find();
        });
    }
    create(newData) {
        return __awaiter(this, void 0, void 0, function* () {
            const newStore = new store_entity_1.Store();
            newStore.name = newData.name;
            newStore.status = newData.status;
            return this.storeRepository.save(newStore);
        });
    }
    update(idStore, updateData) {
        return __awaiter(this, void 0, void 0, function* () {
            const store = yield this.storeRepository.findOneOrFail(idStore);
            store.name = updateData.name;
            store.status = updateData.status;
            return this.storeRepository.save(store);
        });
    }
    delete(idStore) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.storeRepository.delete(idStore);
        });
    }
};
StoreService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(store_entity_1.Store)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], StoreService);
exports.StoreService = StoreService;
//# sourceMappingURL=store.service.js.map