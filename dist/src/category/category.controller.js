"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const category_1 = require("./model/category");
const category_service_1 = require("./category.service");
const auth_guard_1 = require("src/auth.guard");
let CategoryController = class CategoryController {
    constructor(categoryService) {
        this.categoryService = categoryService;
    }
    findAll(response) {
        this.categoryService.findAll().then(data => {
            response.status(common_1.HttpStatus.OK).json(data);
        }).catch(error => {
            response.status(common_1.HttpStatus.FORBIDDEN).json({ msg: 'No se pudo obtener data' });
        });
    }
    create(newCategory, response) {
        this.categoryService.create(newCategory).then(data => {
            response.status(common_1.HttpStatus.CREATED).json(data);
        }).catch(error => {
            response.status(common_1.HttpStatus.FORBIDDEN).json({ msg: 'No se pudo crear la data' });
        });
    }
    update(updateCategory, response, idCategory) {
        this.categoryService.update(idCategory, updateCategory).then(data => {
            response.status(common_1.HttpStatus.OK).json(data);
        }).catch(error => {
            response.status(common_1.HttpStatus.FORBIDDEN).json({ msg: 'No se pudo actualizar la data' });
        });
    }
    delete(response, idCategory) {
        this.categoryService.delete(idCategory).then(data => {
            response.status(common_1.HttpStatus.OK).json(data);
        }).catch(error => {
            response.status(common_1.HttpStatus.FORBIDDEN).json({ msg: 'No se pudo borrar la data' });
        });
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(new auth_guard_1.AuthGuard()),
    __param(0, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], CategoryController.prototype, "findAll", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(new auth_guard_1.AuthGuard()),
    __param(0, common_1.Body()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [category_1.CategoryModel, Object]),
    __metadata("design:returntype", void 0)
], CategoryController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(new auth_guard_1.AuthGuard()),
    __param(0, common_1.Body()), __param(1, common_1.Res()), __param(2, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [category_1.CategoryModel, Object, Object]),
    __metadata("design:returntype", void 0)
], CategoryController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(new auth_guard_1.AuthGuard()),
    __param(0, common_1.Res()), __param(1, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], CategoryController.prototype, "delete", null);
CategoryController = __decorate([
    common_1.Controller('category'),
    __metadata("design:paramtypes", [category_service_1.CategoryService])
], CategoryController);
exports.CategoryController = CategoryController;
//# sourceMappingURL=category.controller.js.map