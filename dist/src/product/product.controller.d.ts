import { ProductModel } from './model/product';
import { ProductService } from './product.service';
export declare class ProductController {
    private productService;
    constructor(productService: ProductService);
    findAll(response: any): void;
    create(newProduct: ProductModel, response: any): void;
    update(updateProduct: ProductModel, response: any, idProduct: any): void;
    delete(idProduct: any): void;
}
