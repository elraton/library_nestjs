export declare class UserModel {
    id: number;
    name: string;
    username: string;
    token?: string;
    password?: string;
}
