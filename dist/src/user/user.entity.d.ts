import { UserModel } from './model/user';
export declare class User {
    id: number;
    name: string;
    username: string;
    hashPassword(): Promise<void>;
    password: string;
    comparePassword(attempt: string): Promise<boolean>;
    toResponseObject(showToken?: boolean): UserModel;
    private readonly token;
}
