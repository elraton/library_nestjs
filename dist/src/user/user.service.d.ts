import { Repository } from 'typeorm';
import { User } from './user.entity';
import { UserModel } from './model/user';
export declare class UserService {
    private readonly userRepository;
    constructor(userRepository: Repository<User>);
    create(newData: UserModel): Promise<User>;
    update(idUser: number, updateData: UserModel): Promise<User>;
    delete(idUser: number): Promise<import("typeorm").DeleteResult>;
    findByUsername(username: string): Promise<User>;
    findById(id: number): Promise<User>;
    login(data: UserModel): Promise<UserModel>;
}
