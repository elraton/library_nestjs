import { Store } from './store.entity';
import { Repository } from 'typeorm';
import { StoreModel } from './model/store';
export declare class StoreService {
    private readonly storeRepository;
    constructor(storeRepository: Repository<Store>);
    findAll(): Promise<Store[]>;
    create(newData: StoreModel): Promise<Store>;
    update(idStore: number, updateData: StoreModel): Promise<Store>;
    delete(idStore: number): Promise<import("typeorm").DeleteResult>;
}
