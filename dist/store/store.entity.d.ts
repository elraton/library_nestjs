import { Product } from '../product/product.entity';
export declare class Store {
    id: number;
    name: string;
    status: string;
    products: Product[];
}
