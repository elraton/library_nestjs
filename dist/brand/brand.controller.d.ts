import { BrandModel } from './model/brand';
import { BrandService } from './brand.service';
export declare class BrandController {
    private brandService;
    constructor(brandService: BrandService);
    findAll(response: any): void;
    create(newBrand: BrandModel, response: any): void;
    update(updateBrand: BrandModel, response: any, idBrand: any): void;
    delete(response: any, idBrand: any): void;
}
