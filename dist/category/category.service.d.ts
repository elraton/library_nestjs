import { Category } from './category.entity';
import { Repository } from 'typeorm';
import { CategoryModel } from './model/category';
export declare class CategoryService {
    private readonly categoryRepository;
    constructor(categoryRepository: Repository<Category>);
    findAll(): Promise<Category[]>;
    create(newData: CategoryModel): Promise<Category>;
    update(idcategory: number, updateData: CategoryModel): Promise<Category>;
    delete(idcategory: number): Promise<import("typeorm").DeleteResult>;
}
