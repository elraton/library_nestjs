import { CategoryModel } from './model/category';
import { CategoryService } from './category.service';
export declare class CategoryController {
    private categoryService;
    constructor(categoryService: CategoryService);
    findAll(response: any): void;
    create(newCategory: CategoryModel, response: any): void;
    update(updateCategory: CategoryModel, response: any, idCategory: any): void;
    delete(response: any, idCategory: any): void;
}
