import { Product } from '../product/product.entity';
export declare class Category {
    id: number;
    name: string;
    status: string;
    products: Product[];
}
