"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const typeorm_1 = require("@nestjs/typeorm");
const store_controller_1 = require("./store/store.controller");
const store_service_1 = require("./store/store.service");
const brand_service_1 = require("./brand/brand.service");
const category_controller_1 = require("./category/category.controller");
const category_service_1 = require("./category/category.service");
const product_controller_1 = require("./product/product.controller");
const product_service_1 = require("./product/product.service");
const brand_controller_1 = require("./brand/brand.controller");
const store_entity_1 = require("./store/store.entity");
const brand_entity_1 = require("./brand/brand.entity");
const category_entity_1 = require("./category/category.entity");
const product_entity_1 = require("./product/product.entity");
const user_controller_1 = require("./user/user.controller");
const user_service_1 = require("./user/user.service");
const user_entity_1 = require("./user/user.entity");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'mysql',
                host: 'localhost',
                port: 3306,
                username: 'root',
                password: 'root',
                database: 'libreria',
                entities: [__dirname + '/**/*.entity{.ts,.js}'],
                synchronize: true,
            }),
            typeorm_1.TypeOrmModule.forFeature([store_entity_1.Store, brand_entity_1.Brand, category_entity_1.Category, product_entity_1.Product, user_entity_1.User]),
        ],
        controllers: [app_controller_1.AppController, brand_controller_1.BrandController, store_controller_1.StoreController, category_controller_1.CategoryController, product_controller_1.ProductController, user_controller_1.UserController],
        providers: [app_service_1.AppService, store_service_1.StoreService, brand_service_1.BrandService, category_service_1.CategoryService, product_service_1.ProductService, user_service_1.UserService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map