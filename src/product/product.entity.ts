import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Category } from '../category/category.entity';
import { Brand } from '../brand/brand.entity';
import { Store } from '../store/store.entity';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  image: string;
  
  @Column({ length: 50 })
  name: string;

  @Column({ length: 50 })
  code: string;

  @Column()
  price: number;

  @Column()
  unit_price: number;

  @Column()
  quantity: number;

  @Column()
  description: string;
  
  @ManyToOne(type => Category, category => category.products)
  category: Category;

  @ManyToOne(type => Brand, brand => brand.products)
  brand: Brand;

  @ManyToOne(type => Store, store => store.products)
  store: Store;

}