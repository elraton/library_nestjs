import { Controller, Get, Post, Put, Delete, Body, Res, HttpStatus, Param, UseGuards } from '@nestjs/common';
import { ProductModel } from './model/product';
import { ProductService } from './product.service';
import { response } from 'express';
import { AuthGuard } from '../auth.guard';

@Controller('product')
export class ProductController {
    constructor(
        private productService: ProductService
    ){}

    @Get()
    @UseGuards(new AuthGuard())
    findAll( @Res() response ) {
        this.productService.findAll().then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo obtener la data'});
            }
        );
    }

    @Post()
    @UseGuards(new AuthGuard())
    create( @Body() newProduct: ProductModel, @Res() response ) {
        this.productService.create(newProduct).then(
            data => {
                response.status(HttpStatus.CREATED).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo crear la data'});
            }
        );
    }

    @Put(':id')
    @UseGuards(new AuthGuard())
    update( @Body() updateProduct: ProductModel, @Res() response, @Param('id') idProduct ) {
        this.productService.update(idProduct, updateProduct).then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo actualizar la data'});
            }
        );
    }

    @Delete(':id')
    @UseGuards(new AuthGuard())
    delete( @Param('id') idProduct ) {
        this.productService.delete(idProduct).then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo actualizar la data'});
            }
        );
    }
}
