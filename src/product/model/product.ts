export class ProductModel {
    id: number;
    image: string;
    name: string;
    code: string;
    price: number;
    unit_price: number;
    quantity: number;
    description: string;
    category: number;
    brand: number;
    store: number;
}
