import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './product.entity';
import { Repository } from 'typeorm';
import { ProductModel } from './model/product';
import { Category } from '../category/category.entity';
import { Brand } from '../brand/brand.entity';
import { Store } from '../store/store.entity';

@Injectable()
export class ProductService {
    constructor(
        @InjectRepository(Product)
        private readonly productRepository: Repository<Product>,
        
        @InjectRepository(Category)
        private readonly categoryRepository: Repository<Category>,

        @InjectRepository(Brand)
        private readonly brandRepository: Repository<Brand>,

        @InjectRepository(Store)
        private readonly storeRepository: Repository<Store>
    ) {}

    async findAll(): Promise<Product[]> {
        return await this.productRepository.find();
    }

    async create(newData: ProductModel): Promise<Product> {
        const newcategory = new Product();
        newcategory.name = newData.name;
        newcategory.code = newData.code;
        newcategory.image = newData.image;
        newcategory.price = newData.price;
        newcategory.unit_price = newData.unit_price;
        newcategory.quantity = newData.quantity;
        newcategory.description = newData.description;
        newcategory.category = await this.categoryRepository.findOneOrFail(newData.category);
        newcategory.brand = await this.brandRepository.findOneOrFail(newData.brand);
        newcategory.store = await this.storeRepository.findOneOrFail(newData.store);
        return this.productRepository.save(newcategory);
    }
    
    async update(idproduct: number, updateData: ProductModel) {
        const product = await this.productRepository.findOneOrFail(idproduct);
        product.name = updateData.name;
        product.code = updateData.code;
        product.image = updateData.image;
        product.price = updateData.price;
        product.unit_price = updateData.unit_price;
        product.quantity = updateData.quantity;
        product.description = updateData.description;
        product.category = await this.categoryRepository.findOneOrFail(updateData.category);
        product.brand = await this.brandRepository.findOneOrFail(updateData.brand);
        product.store = await this.storeRepository.findOneOrFail(updateData.store);
        return this.productRepository.save(product);
    }

    async delete(idproduct: number) {
        return await this.productRepository.delete(idproduct);
        
    }
}
