import { Controller, Get, Post, Put, Delete, Body, Res, HttpStatus, Param, UseGuards } from '@nestjs/common';
import { BrandModel } from './model/brand';
import { BrandService } from './brand.service';
import { AuthGuard } from '../auth.guard';

@Controller('brand')
export class BrandController {

    constructor(
        private brandService: BrandService,
    ) {}
    
    @Get()
    @UseGuards(new AuthGuard())
    findAll( @Res() response) {
        this.brandService.findAll().then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo obtener data'});
            }
        );
    }

    @Post()
    @UseGuards(new AuthGuard())
    create( @Body() newBrand: BrandModel, @Res() response ) {
        this.brandService.create(newBrand).then(
            data => {
                response.status(HttpStatus.CREATED).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo crear la data'});
            }
        );
    }

    @Put(':id')
    @UseGuards(new AuthGuard())
    update( @Body() updateBrand: BrandModel, @Res() response, @Param('id') idBrand ) {
        this.brandService.update(idBrand, updateBrand).then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo actualizar la data'});
            }
        );
    }

    @Delete(':id')
    @UseGuards(new AuthGuard())
    delete( @Res() response, @Param('id') idBrand ) {
        this.brandService.delete(idBrand).then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo borrar la data'});
            }
        );
    }
}