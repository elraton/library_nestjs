import { Injectable } from '@nestjs/common';
import { BrandModel } from './model/brand';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Brand } from './brand.entity';

@Injectable()
export class BrandService {
    constructor(
        @InjectRepository(Brand)
        private readonly brandRepository: Repository<Brand>
    ) {}

    async findAll(): Promise<Brand[]> {
        return await this.brandRepository.find();
    }

    async create(newData: BrandModel): Promise<Brand> {
        const newbrand = new Brand();
        newbrand.name = newData.name;
        newbrand.status = newData.status;
        return this.brandRepository.save(newbrand);
    }
    
    async update(idbrand: number, updateData: BrandModel) {
        const brand = await this.brandRepository.findOneOrFail(idbrand);
        brand.name = updateData.name;
        brand.status = updateData.status;
        return this.brandRepository.save(brand);
    }

    async delete(idBrand: number) {
        return await this.brandRepository.delete(idBrand);
        
    }
}
