import { Controller, Get, Post, Put, Delete, Body, Res, HttpStatus, Param, UseGuards } from '@nestjs/common';
import { CategoryModel } from './model/category';
import { CategoryService } from './category.service';
import { response } from 'express';
import { AuthGuard } from '../auth.guard';

@Controller('category')
export class CategoryController {
    constructor(
        private categoryService: CategoryService,
    ) {}
    @Get()
    @UseGuards(new AuthGuard())
    findAll( @Res() response ) {
        this.categoryService.findAll().then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo obtener data'});
            }
        );
    }

    @Post()
    @UseGuards(new AuthGuard())
    create( @Body() newCategory: CategoryModel, @Res() response ) {
        this.categoryService.create(newCategory).then(
            data => {
                response.status(HttpStatus.CREATED).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo crear la data'});
            }
        );
    }

    @Put(':id')
    @UseGuards(new AuthGuard())
    update( @Body() updateCategory: CategoryModel, @Res() response, @Param('id') idCategory ) {
        this.categoryService.update(idCategory, updateCategory).then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo actualizar la data'});
            }
        );
    }

    @Delete(':id')
    @UseGuards(new AuthGuard())
    delete( @Res() response, @Param('id') idCategory ) {
        this.categoryService.delete(idCategory).then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo borrar la data'});
            }
        );
    }
}
