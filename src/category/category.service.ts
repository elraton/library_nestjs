import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './category.entity';
import { Repository } from 'typeorm';
import { CategoryModel } from './model/category';

@Injectable()
export class CategoryService {
    constructor(
        @InjectRepository(Category)
        private readonly categoryRepository: Repository<Category>
    ) {}

    async findAll(): Promise<Category[]> {
        return await this.categoryRepository.find();
    }

    async create(newData: CategoryModel): Promise<Category> {
        const newcategory = new Category();
        newcategory.name = newData.name;
        newcategory.status = newData.status;
        return this.categoryRepository.save(newcategory);
    }
    
    async update(idcategory: number, updateData: CategoryModel) {
        const category = await this.categoryRepository.findOneOrFail(idcategory);
        category.name = updateData.name;
        category.status = updateData.status;
        return this.categoryRepository.save(category);
    }

    async delete(idcategory: number) {
        return await this.categoryRepository.delete(idcategory);
        
    }
}
