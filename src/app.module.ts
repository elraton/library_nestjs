import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StoreController } from './store/store.controller';
import { StoreService } from './store/store.service';
import { BrandService } from './brand/brand.service';
import { CategoryController } from './category/category.controller';
import { CategoryService } from './category/category.service';
import { ProductController } from './product/product.controller';
import { ProductService } from './product/product.service';
import { BrandController } from './brand/brand.controller';
import { Store } from './store/store.entity';
import { Brand } from './brand/brand.entity';
import { Category } from './category/category.entity';
import { Product } from './product/product.entity';
import { UserController } from './user/user.controller';
import { UserService } from './user/user.service';
import { User } from './user/user.entity';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'libreria',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Store, Brand, Category, Product, User]),
  ],
  controllers: [AppController, BrandController, StoreController, CategoryController, ProductController, UserController],
  providers: [AppService, StoreService, BrandService, CategoryService, ProductService, UserService],
})
export class AppModule {}