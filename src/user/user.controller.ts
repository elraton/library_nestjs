import { Controller, Get, Res, UseGuards, Post, Body, HttpStatus } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.decorator';
import { AuthGuard } from '../auth.guard';
import { UserModel } from './model/user';

@Controller('user')
export class UserController {
    constructor(
        private userService: UserService
    ) {

    }

    @Get('auth/whoami')
    @UseGuards(new AuthGuard())
    showMe(@User('username') username: string) {
        return this.userService.findByUsername(username);
    }

    @Post()
    register( @Body() newUser: UserModel, @Res() response ) {
        this.userService.create(newUser).then(
            data => {
                response.status(HttpStatus.CREATED).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo guardar la data'});
            }
        );
    }

    @Post()
    login(@Body() data: UserModel) {
        return this.userService.login(data);
    }
}
