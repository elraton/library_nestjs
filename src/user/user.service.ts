import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { UserModel } from './model/user';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>
    ) {}

    async create(newData: UserModel): Promise<User> {
        const newUser = new User();
        newUser.name = newData.name;
        newUser.username = newData.username;
        newUser.password = newData.password;
        return this.userRepository.save(newUser);
    }
    
    async update(idUser: number, updateData: UserModel) {
        const user = await this.userRepository.findOneOrFail(idUser);
        user.name = updateData.name;
        user.username = updateData.username;
        return this.userRepository.save(user);
    }

    async delete(idUser: number) {
        return await this.userRepository.delete(idUser);        
    }

    async findByUsername(username: string): Promise<User> {
        return await this.userRepository.findOne({
            where: {
                username: username,
            }
        });
    }

    async findById(id: number): Promise<User> {
        return await this.userRepository.findOne({
            where: {
                id: id,
            }
        });
    }

    async login(data: UserModel) {
        const { username, password } = data;
        const user = await this.userRepository.findOne({ where: { username } });
        if (!user || !(await user.comparePassword(password))) {
            throw new HttpException(
                'Invalid username/password',
                HttpStatus.BAD_REQUEST,
            );
        }
        return user.toResponseObject();
    }
}
