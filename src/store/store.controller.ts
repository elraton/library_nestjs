import { Controller, Get, Post, Put, Delete, Body, Res, HttpStatus, Param, UseGuards } from '@nestjs/common';
import { StoreModel } from './model/store';
import { StoreService } from './store.service';
import { AuthGuard } from '../auth.guard';

@Controller('store')
export class StoreController {

    constructor(
        private storeService: StoreService
    ) {

    }
    
    @Get()
    @UseGuards(new AuthGuard())
    findAll( @Res() response) {
        this.storeService.findAll().then(
            data => {
                response.status(HttpStatus.OK).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo obtener data'});
            }
        );
    }

    @Post()
    @UseGuards(new AuthGuard())
    create( @Body() newStore: StoreModel, @Res() response ) {
        this.storeService.create(newStore).then(
            data => {
                response.status(HttpStatus.CREATED).json(data);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo guardar la data'});
            }
        );
    }

    @Put(':id')
    @UseGuards(new AuthGuard())
    update( @Body() updateStore: StoreModel, @Res() response, @Param('id') idStore) {
        this.storeService.update(idStore, updateStore).then(
            data => {
                response.status(HttpStatus.OK);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo actualizar la data'});
            }
        );
    }

    @Delete(':id')
    @UseGuards(new AuthGuard())
    delete( @Res() response, @Param('id') idStore ) {
        this.storeService.delete(idStore).then(
            data => {
                response.status(HttpStatus.OK);
            }
        ).catch(
            error => {
                response.status(HttpStatus.FORBIDDEN).json({msg: 'No se pudo borrar la data'});
            }
        );
    }
}