import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Store } from './store.entity';
import { Repository } from 'typeorm';
import { StoreModel } from './model/store';

@Injectable()
export class StoreService {
    constructor(
        @InjectRepository(Store)
        private readonly storeRepository: Repository<Store>
    ) {}

    async findAll(): Promise<Store[]> {
        return await this.storeRepository.find();
    }

    async create(newData: StoreModel): Promise<Store> {
        const newStore = new Store();
        newStore.name = newData.name;
        newStore.status = newData.status;
        return this.storeRepository.save(newStore);
    }
    
    async update(idStore: number, updateData: StoreModel) {
        const store = await this.storeRepository.findOneOrFail(idStore);
        store.name = updateData.name;
        store.status = updateData.status;
        return this.storeRepository.save(store);
    }

    async delete(idStore: number) {
        return await this.storeRepository.delete(idStore);
        
    }
}
